import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        Scanner sc = new Scanner(System.in);
        int type = sc.nextInt();
        System.out.print("Please input number: ");
        int n = sc.nextInt();
        if (type == 1) { // type1
            for (int i = 0; i < n; i++) {
                for (int j = 0; j <= i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (type == 2) { // type2
            for (int i = n; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (type == 3) { // type3
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < i; j++) {
                    System.out.print(" ");
                }
                for (int j = 0; j < n - i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (type == 4) {
            for (int i = 4; i >= 0; i--) {
                for (int j = 0; j < n; j++) {
                    if (j >= i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        } else if (type == 5) {
            System.out.print("Bye bye!!!");
        } else {
            System.out.println("Error: Please input number between 1-5");

        }
    }
}
